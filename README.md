## MLflow Infra Project

This project provides a Dockerized MLflow environment with PostgreSQL and MinIO for tracking and storing machine learning experiments.

**Getting Started**

1. Clone the repository:
    ```sh
    git clone https://gitlab.com/itmo1073917/ITMO_MLFlow_Infra
    ```
2. Create a `.env` file with your environment variables (see `.env_example` file for example)
3. Run this command to start the containers
   ```sh
   docker-compose up
   ```
4. Access MLflow at `http://localhost:5000`

**Services**

The project consists of three services:

* `mlflow`: The MLflow server, built from the `Dockerfile` in this repository
* `db`: A PostgreSQL database for storing MLflow tracking data
* `minio`: A MinIO server for storing MLflow artifacts

**Environment Variables**

The following environment variables are required:

* `AWS_ACCESS_KEY_ID`: MinIO access key ID
* `AWS_SECRET_ACCESS_KEY`: MinIO secret access key
* `MLFLOW_TRACKING_URI`: URI for the MLflow tracking store (e.g. PostgreSQL database)
* `MLFLOW_ARTIFACT_ROOT`: Root directory for MLflow artifacts (e.g. S3 bucket)
* `POSTGRES_DB`: PostgreSQL database name
* `POSTGRES_USER`: PostgreSQL username
* `POSTGRES_PASSWORD`: PostgreSQL password

**Docker Compose**

The `docker-compose.yaml` file defines the services and their dependencies. It also sets up the environment variables and volumes for each service.

**Entrypoint Script**

The `entrypoint.sh` script is used to start the MLflow server with the correct configuration.

**License**

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).
