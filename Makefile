MLFLOW_SERVICE ?= mlflow-infra.service

deploy-mlflow-infra:
	sudo systemctl stop "$(MLFLOW_SERVICE)" || true
	sudo rm -rf /opt/mlflow-infra
	sudo mkdir -p /opt/mlflow-infra
	sudo cp "$(ENV_FILE)" /opt/mlflow-infra/.env
	sudo cp Dockerfile entrypoint.sh docker-compose.yml prometheus.yml /opt/mlflow-infra
	sudo cp "$(MLFLOW_SERVICE)" /etc/systemd/system/
	sudo systemctl daemon-reload
	sudo systemctl enable "$(MLFLOW_SERVICE)"
	sudo systemctl start "$(MLFLOW_SERVICE)"